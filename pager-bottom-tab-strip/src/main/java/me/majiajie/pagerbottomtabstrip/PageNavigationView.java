package me.majiajie.pagerbottomtabstrip;

import java.util.ArrayList;
import java.util.List;

import me.majiajie.pagerbottomtabstrip.internal.CustomItemVerticalLayout;
import me.majiajie.pagerbottomtabstrip.internal.MaterialItemVerticalLayout;
import me.majiajie.pagerbottomtabstrip.item.OnlyIconMaterialItemView;
import me.majiajie.pagerbottomtabstrip.util.MyValueAnimator;
import me.majiajie.pagerbottomtabstrip.util.ResourceUtil;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.app.Context;
import ohos.agp.utils.Color;
import ohos.agp.components.PageSlider;
import ohos.agp.components.ComponentContainer;
import me.majiajie.pagerbottomtabstrip.internal.CustomItemLayout;
import me.majiajie.pagerbottomtabstrip.internal.MaterialItemLayout;
import me.majiajie.pagerbottomtabstrip.internal.Utils;
import me.majiajie.pagerbottomtabstrip.item.BaseTabItem;
import me.majiajie.pagerbottomtabstrip.item.MaterialItemView;
import me.majiajie.pagerbottomtabstrip.listener.OnTabItemSelectedListener;
import ohos.media.image.PixelMap;

/**
 * 导航视图
 */
public class PageNavigationView extends ComponentContainer {

    private static final String NAVIGATION_PADDING_TOP = "navigationPaddingTop";
    private static final String NAVIGATION_PADDING_BOTTOM = "navigationPaddingBottom";

    private int mTabPaddingTop;
    private int mTabPaddingBottom;

    private NavigationController mNavigationController;

    private PageSliderPageChangedListener mPageChangeListener;
    private PageSlider mViewPager;

    private boolean mEnableVerticalLayout;

    private OnTabItemSelectedListener mTabItemListener = new OnTabItemSelectedListener() {
        @Override
        public void onSelected(int index, int old) {
            if (mViewPager != null) {
                mViewPager.setCurrentPage(index, false);
            }
        }

        @Override
        public void onRepeat(int index) {
        }
    };

    public PageNavigationView(Context context) {
        this(context, null);
    }

    public PageNavigationView(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public PageNavigationView(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
        setPadding(0, 0, 0, 0);
        mTabPaddingTop = attrs.getAttr(NAVIGATION_PADDING_TOP).isPresent() ? attrs.getAttr(NAVIGATION_PADDING_TOP).get().getDimensionValue() : 0;
        mTabPaddingBottom = attrs.getAttr(NAVIGATION_PADDING_BOTTOM).isPresent() ? attrs.getAttr(NAVIGATION_PADDING_BOTTOM).get().getDimensionValue() : 0;
    }

    /**
     * 构建 Material Desgin 风格的导航栏
     *
     * @return Material Desgin 风格的导航栏
     */
    public MaterialBuilder material() {
        return new MaterialBuilder();
    }

    /**
     * 构建自定义导航栏
     *
     * @return 自定义导航栏
     */
    public CustomBuilder custom() {
        return new CustomBuilder();
    }

    /**
     * 构建 自定义 的导航栏
     */
    public class CustomBuilder {

        private List<BaseTabItem> items;

        private boolean enableVerticalLayout = false;
        private boolean animateLayoutChanges = false;

        CustomBuilder() {
            items = new ArrayList<>();
        }

        /**
         * 完成构建
         *
         * @return {@link NavigationController},通过它进行后续操作
         * @throws RuntimeException 没有添加导航项时会抛出
         */
        public NavigationController build() {

            mEnableVerticalLayout = enableVerticalLayout;

            // 未添加任何按钮
            if (items.size() == 0) {
                throw new RuntimeException("must add a navigation item");
            }

            ItemController itemController;

            if (enableVerticalLayout) { // 垂直布局
                CustomItemVerticalLayout verticalItemLayout = new CustomItemVerticalLayout(getContext());
                verticalItemLayout.initialize(items, animateLayoutChanges);
                verticalItemLayout.setPadding(0, mTabPaddingTop, 0, mTabPaddingBottom);

                PageNavigationView.this.removeAllComponents();
                PageNavigationView.this.addComponent(verticalItemLayout);
                itemController = verticalItemLayout;
            } else { // 水平布局
                CustomItemLayout customItemLayout = new CustomItemLayout(getContext());
                customItemLayout.initialize(items, animateLayoutChanges);
                customItemLayout.setPadding(0, mTabPaddingTop, 0, mTabPaddingBottom);

                PageNavigationView.this.removeAllComponents();
                PageNavigationView.this.addComponent(customItemLayout);
                itemController = customItemLayout;

            }

            mNavigationController = new NavigationController(new Controller(), itemController);
            mNavigationController.addTabItemSelectedListener(mTabItemListener);

            return mNavigationController;
        }

        /**
         * 添加一个导航按钮
         *
         * @param baseTabItem {@link BaseTabItem},所有自定义Item都必须继承它
         * @return {@link CustomBuilder}
         */
        public CustomBuilder addItem(BaseTabItem baseTabItem) {
            items.add(baseTabItem);
            return CustomBuilder.this;
        }

        /**
         * 使用垂直布局
         *
         * @return {@link CustomBuilder}
         */
        public CustomBuilder enableVerticalLayout() {
            enableVerticalLayout = true;
            return CustomBuilder.this;
        }

        /**
         * 通过{@link NavigationController}动态移除/添加导航项时,显示默认的布局动画
         *
         * @return {@link CustomBuilder}
         */
        public CustomBuilder enableAnimateLayoutChanges() {
            animateLayoutChanges = true;
            return CustomBuilder.this;
        }
    }

    /**
     * 构建 Material Desgin 风格的导航栏
     */
    public class MaterialBuilder {

        private final int DEFAULT_COLOR = 0x56000000;

        private List<MaterialItemViewData> itemDatas;
        private int defaultColor;
        private int mode;
        private int messageBackgroundColor;
        private int messageNumberColor;
        private boolean enableVerticalLayout = false;
        private boolean doTintIcon = true;
        private boolean animateLayoutChanges = false;

        MaterialBuilder() {
            itemDatas = new ArrayList<>();
        }

        /**
         * 完成构建
         *
         * @return {@link NavigationController},通过它进行后续操作
         * @throws RuntimeException 没有添加导航项时会抛出
         */
        public NavigationController build() {
            mEnableVerticalLayout = enableVerticalLayout;

            // 未添加任何按钮
            if (itemDatas.isEmpty()) {
                throw new RuntimeException("must add a navigation item");
            }

            // 设置默认颜色
            if (defaultColor == 0) {
                defaultColor = DEFAULT_COLOR;
            }

            ItemController itemController;

            if (enableVerticalLayout) { // 垂直布局

                List<BaseTabItem> items = new ArrayList<>();

                for (MaterialItemViewData data : itemDatas) {

                    OnlyIconMaterialItemView materialItemView = new OnlyIconMaterialItemView(getContext());
                    materialItemView.initialization(data.title, data.drawable, data.checkedDrawable, doTintIcon, defaultColor, data.chekedColor);

                    // 检查是否设置了消息圆点的颜色
                    if (messageBackgroundColor != 0) {
                        materialItemView.setMessageBackgroundColor(messageBackgroundColor);
                    }

                    // 检查是否设置了消息数字的颜色
                    if (messageNumberColor != 0) {
                        materialItemView.setMessageNumberColor(messageNumberColor);
                    }

                    items.add(materialItemView);

                }

                MaterialItemVerticalLayout materialItemVerticalLayout = new MaterialItemVerticalLayout(getContext());
                materialItemVerticalLayout.initialize(items, animateLayoutChanges, doTintIcon, defaultColor);
                materialItemVerticalLayout.setPadding(0, mTabPaddingTop, 0, mTabPaddingBottom);

                PageNavigationView.this.removeAllComponents();
                PageNavigationView.this.addComponent(materialItemVerticalLayout);

                itemController = materialItemVerticalLayout;

            } else { // 水平布局

                boolean changeBackground = (mode & MaterialMode.CHANGE_BACKGROUND_COLOR) > 0;

                List<MaterialItemView> items = new ArrayList<>();
                List<Integer> checkedColors = new ArrayList<>();

                for (MaterialItemViewData data : itemDatas) {
                    // 记录设置的选中颜色
                    checkedColors.add(data.chekedColor);

                    MaterialItemView materialItemView = new MaterialItemView(getContext());
                    // 初始化Item,需要切换背景颜色就将选中颜色改成白色
                    materialItemView.initialization(data.title, data.drawable, data.checkedDrawable, doTintIcon, defaultColor, changeBackground ? Color.WHITE.getValue() : data.chekedColor);

                    // 检查是否设置了消息圆点的颜色
                    if (messageBackgroundColor != 0) {
                        materialItemView.setMessageBackgroundColor(messageBackgroundColor);
                    }

                    // 检查是否设置了消息数字的颜色
                    if (messageNumberColor != 0) {
                        materialItemView.setMessageNumberColor(messageNumberColor);
                    }

                    items.add(materialItemView);
                }

                MaterialItemLayout materialItemLayout = new MaterialItemLayout(getContext());
                materialItemLayout.initialize(items, checkedColors, mode, animateLayoutChanges, doTintIcon, defaultColor);
                materialItemLayout.setPadding(0, mTabPaddingTop, 0, mTabPaddingBottom);

                PageNavigationView.this.removeAllComponents();
                PageNavigationView.this.addComponent(materialItemLayout);

                itemController = materialItemLayout;
            }

            mNavigationController = new NavigationController(new Controller(), itemController);
            mNavigationController.addTabItemSelectedListener(mTabItemListener);

            return mNavigationController;
        }

        /**
         * 添加一个导航按钮
         *
         * @param drawableRes 图标资源
         * @param title       显示文字内容.尽量简短
         * @return {@link MaterialBuilder}
         */
        public MaterialBuilder addItem(int drawableRes, String title) {
            addItem(drawableRes, drawableRes, title, Utils.getColorPrimary(getContext()));
            return MaterialBuilder.this;
        }

        /**
         * 添加一个导航按钮
         *
         * @param drawableRes        图标资源
         * @param checkedDrawableRes 选中时的图标资源
         * @param title              显示文字内容.尽量简短
         * @return {@link MaterialBuilder}
         */
        public MaterialBuilder addItem(int drawableRes, int checkedDrawableRes, String title) {
            addItem(drawableRes, checkedDrawableRes, title, Utils.getColorPrimary(getContext()));
            return MaterialBuilder.this;
        }

        /**
         * 添加一个导航按钮
         *
         * @param drawableRes 图标资源
         * @param title       显示文字内容.尽量简短
         * @param chekedColor 选中的颜色
         * @return {@link MaterialBuilder}
         */
        public MaterialBuilder addItem(int drawableRes, String title, int chekedColor) {
            addItem(drawableRes, drawableRes, title, chekedColor);
            return MaterialBuilder.this;
        }

        /**
         * 添加一个导航按钮
         *
         * @param drawableRes        图标资源
         * @param checkedDrawableRes 选中时的图标资源
         * @param title              显示文字内容.尽量简短
         * @param chekedColor        选中的颜色
         * @return {@link MaterialBuilder}
         * @throws RuntimeException drawable 资源获取异常
         */
        public MaterialBuilder addItem(int drawableRes, int checkedDrawableRes, String title, int chekedColor) {
            PixelMap defaultDrawable = ResourceUtil.decodeResource(getContext(), drawableRes);
            PixelMap checkDrawable = ResourceUtil.decodeResource(getContext(), checkedDrawableRes);

            if (defaultDrawable == null) {
                throw new RuntimeException("Resource ID " + Integer.toHexString(drawableRes));
            }
            if (checkDrawable == null) {
                throw new RuntimeException("Resource ID " + Integer.toHexString(checkedDrawableRes));
            }
            addItem(defaultDrawable, checkDrawable, title, chekedColor);
            return MaterialBuilder.this;
        }

        /**
         * 添加一个导航按钮
         *
         * @param drawable 图标资源
         * @param title    显示文字内容.尽量简短
         * @return {@link MaterialBuilder}
         */
        public MaterialBuilder addItem(PixelMap drawable, String title) {
            addItem(drawable, drawable, title, Utils.getColorPrimary(getContext()));
            return MaterialBuilder.this;
        }

        /**
         * 添加一个导航按钮
         *
         * @param drawable        图标资源
         * @param checkedDrawable 选中时的图标资源
         * @param title           显示文字内容.尽量简短
         * @return {@link MaterialBuilder}
         */
        public MaterialBuilder addItem(PixelMap drawable, PixelMap checkedDrawable, String title) {
            addItem(drawable, checkedDrawable, title, Utils.getColorPrimary(getContext()));
            return MaterialBuilder.this;
        }

        /**
         * 添加一个导航按钮
         *
         * @param drawable    图标资源
         * @param title       显示文字内容.尽量简短
         * @param chekedColor 选中的颜色
         * @return {@link MaterialBuilder}
         */
        public MaterialBuilder addItem(PixelMap drawable, String title, int chekedColor) {
            addItem(drawable, drawable, title, chekedColor);
            return MaterialBuilder.this;
        }

        /**
         * 添加一个导航按钮
         *
         * @param drawable        图标资源
         * @param checkedDrawable 选中时的图标资源
         * @param title           显示文字内容.尽量简短
         * @param chekedColor     选中的颜色
         * @return {@link MaterialBuilder}
         */
        public MaterialBuilder addItem(PixelMap drawable, PixelMap checkedDrawable, String title, int chekedColor) {
            MaterialItemViewData data = new MaterialItemViewData();
            data.drawable = Utils.newDrawable(drawable);
            data.checkedDrawable = Utils.newDrawable(checkedDrawable);
            data.title = title;
            data.chekedColor = chekedColor;
            itemDatas.add(data);
            return MaterialBuilder.this;
        }

        /**
         * 设置导航按钮的默认（未选中状态）颜色
         *
         * @param color 16进制整形表示的颜色，例如红色：0xFFFF0000
         * @return {@link MaterialBuilder}
         */
        public MaterialBuilder setDefaultColor(int color) {
            defaultColor = color;
            return MaterialBuilder.this;
        }

        /**
         * 设置消息圆点的颜色
         *
         * @param color 16进制整形表示的颜色，rgba，例如红色：0xFF0000FF
         * @return {@link MaterialBuilder}
         */
        public MaterialBuilder setMessageBackgroundColor(int color) {
            messageBackgroundColor = color;
            return MaterialBuilder.this;
        }

        /**
         * 设置消息数字的颜色
         *
         * @param color 16进制整形表示的颜色，例如红色：0xFFFF0000
         * @return {@link MaterialBuilder}
         */
        public MaterialBuilder setMessageNumberColor(int color) {
            messageNumberColor = color;
            return MaterialBuilder.this;
        }

        /**
         * 设置模式(在垂直布局中无效)。默认文字一直显示，且背景色不变。
         * 可以通过{@link MaterialMode}选择模式。
         * <p>例如:</p>
         * {@code MaterialMode.HIDE_TEXT}
         * <p>或者多选:</p>
         * {@code MaterialMode.HIDE_TEXT | MaterialMode.CHANGE_BACKGROUND_COLOR}
         *
         * @param mode {@link MaterialMode}
         * @return {@link MaterialBuilder}
         */
        public MaterialBuilder setMode(int mode) {
            MaterialBuilder.this.mode = mode;
            return MaterialBuilder.this;
        }

        /**
         * 使用垂直布局
         *
         * @return {@link MaterialBuilder}
         */
        public MaterialBuilder enableVerticalLayout() {
            enableVerticalLayout = true;
            return MaterialBuilder.this;
        }

        /**
         * 不对图标进行染色
         *
         * @return {@link MaterialBuilder}
         */
        public MaterialBuilder dontTintIcon() {
            doTintIcon = false;
            return MaterialBuilder.this;
        }

        /**
         * 通过{@link NavigationController}动态移除/添加导航项时,显示默认的布局动画
         *
         * @return {@link MaterialBuilder}
         */
        public MaterialBuilder enableAnimateLayoutChanges() {
            animateLayoutChanges = true;
            return MaterialBuilder.this;
        }

    }

    /**
     * 材料设计的单项视图信息
     */
    private static class MaterialItemViewData {
        PixelMap drawable;
        PixelMap checkedDrawable;
        String title;
        int chekedColor;
    }

    /**
     * 实现控制接口
     */
    private class Controller implements BottomLayoutController {

        private MyValueAnimator animator;
        private boolean hide = false;
        private int showBottom = 0;

        @Override
        public void setupWithViewPager(PageSlider viewPager) {
            if (viewPager == null) {
                return;
            }

            mViewPager = viewPager;

            if (mPageChangeListener != null) {
                mViewPager.removePageChangedListener(mPageChangeListener);
            } else {
                mPageChangeListener = new PageSliderPageChangedListener();
            }

            if (mNavigationController != null) {
                int current = mViewPager.getCurrentPage();
                if (mNavigationController.getSelected() != current) {
                    mNavigationController.setSelect(current);
                }
                mViewPager.addPageChangedListener(mPageChangeListener);
            }
        }

        @Override
        public void hideBottomLayout() {
            if (!hide) {
                hide = true;
                getAnimator().start();
            }
        }

        @Override
        public void showBottomLayout() {
            if (hide) {
                hide = false;
                getAnimator().reverse();
            }
        }

        private MyValueAnimator getAnimator() {
            if (animator == null) {
                if (mEnableVerticalLayout) { // 垂直布局向左隐藏
                    animator = MyValueAnimator.ofFloat(0, 1);
                    animator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                        @Override
                        public void onUpdate(AnimatorValue animatorValue, float value) {
                            PageNavigationView.this.setMarginLeft(-(int) (PageNavigationView.this.getWidth() * value));
                        }
                    });
                } else { // 水平布局向下隐藏
                    animator = MyValueAnimator.ofFloat(0, 1);
                    animator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                        @Override
                        public void onUpdate(AnimatorValue animatorValue, float value) {
                            PageNavigationView.this.setMarginBottom(-(int) (PageNavigationView.this.getHeight() * value));
                        }
                    });
                }
                animator.setDuration(300);
                animator.setCurveType(AnimatorProperty.CurveType.ACCELERATE_DECELERATE);
            }

            return animator;
        }

    }

    private class PageSliderPageChangedListener implements PageSlider.PageChangedListener {

        @Override
        public void onPageSliding(int position, float itemPosOffset, int itemPosOffsetPixels) {

        }

        @Override
        public void onPageChosen(int itemPos) {
            if (mNavigationController != null && mNavigationController.getSelected() != itemPos) {
                mNavigationController.setSelect(itemPos);
            }
        }

        @Override
        public void onPageSlideStateChanged(int state) {

        }

    }

}
