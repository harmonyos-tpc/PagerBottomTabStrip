package me.majiajie.pagerbottomtabstrip.item;

import me.majiajie.pagerbottomtabstrip.ResourceTable;
import me.majiajie.pagerbottomtabstrip.internal.RoundMessageView;
import me.majiajie.pagerbottomtabstrip.util.ResourceUtil;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.media.image.PixelMap;

public class NormalItemView extends BaseTabItem {

    private Image mIcon;
    private final Text mTitle;
    private final RoundMessageView mMessages;

    private PixelMap mDefaultDrawable;
    private PixelMap mCheckedDrawable;

    private int mDefaultTextColor = 0x56000000;
    private int mCheckedTextColor = 0x56000000;

    private boolean mChecked;

    public NormalItemView(Context context) {
        this(context, null);
    }

    public NormalItemView(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public NormalItemView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);

        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item_normal, this, true);

        mIcon = (Image) findComponentById(ResourceTable.Id_icon);
        mTitle = (Text) findComponentById(ResourceTable.Id_title);
        mMessages = (RoundMessageView) findComponentById(ResourceTable.Id_messages);
    }

    /**
     * 方便初始化的方法
     *
     * @param drawableRes        默认状态的图标
     * @param checkedDrawableRes 选中状态的图标
     * @param title              标题
     */
    public void initialize(int drawableRes, int checkedDrawableRes, String title) {
        mDefaultDrawable = ResourceUtil.decodeResource(getContext(), drawableRes);
        mCheckedDrawable = ResourceUtil.decodeResource(getContext(), checkedDrawableRes);
        mTitle.setText(title);
    }

    @Override
    public void setChecked(boolean checked) {
        if (checked) {
            mIcon.setPixelMap(mCheckedDrawable);
            mTitle.setTextColor(new Color(mCheckedTextColor));
        } else {
            mIcon.setPixelMap(mDefaultDrawable);
            mTitle.setTextColor(new Color(mDefaultTextColor));
        }
        mChecked = checked;
    }

    @Override
    public void setMessageNumber(int number) {
        mMessages.setMessageNumber(number);
    }

    @Override
    public void setHasMessage(boolean hasMessage) {
        mMessages.setHasMessage(hasMessage);
    }

    @Override
    public void setTitle(String title) {
        mTitle.setText(title);
    }

    @Override
    public void setDefaultDrawable(PixelMap drawable) {
        mDefaultDrawable = drawable;
        if (!mChecked) {
            mIcon.setPixelMap(drawable);
        }
    }

    @Override
    public void setSelectedDrawable(PixelMap drawable) {
        mCheckedDrawable = drawable;
        if (mChecked) {
            mIcon.setPixelMap(drawable);
        }
    }

    @Override
    public String getTitle() {
        return mTitle.getText().toString();
    }

    public void setTextDefaultColor(int color) {
        mDefaultTextColor = color;
    }

    public void setTextCheckedColor(int color) {
        mCheckedTextColor = color;
    }

}
