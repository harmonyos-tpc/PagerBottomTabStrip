package me.majiajie.pagerbottomtabstrip.item;

import me.majiajie.pagerbottomtabstrip.ResourceTable;
import me.majiajie.pagerbottomtabstrip.internal.RoundMessageView;
import me.majiajie.pagerbottomtabstrip.internal.Utils;
import me.majiajie.pagerbottomtabstrip.widget.RippleLayout;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;
import ohos.media.image.PixelMap;

/**
 * 只有图标的材料设计项(用于垂直布局)
 */
public class OnlyIconMaterialItemView extends BaseTabItem {

    private final RoundMessageView mMessages;
    private final Image mIcon;
    private final RippleLayout ripple;

    private PixelMap mDefaultDrawable;
    private PixelMap mCheckedDrawable;

    private int mDefaultColor;
    private int mCheckedColor;

    private String mTitle;

    private boolean mChecked;

    private boolean mTintIcon = true;

    public OnlyIconMaterialItemView(Context context) {
        this(context, null);
    }

    public OnlyIconMaterialItemView(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public OnlyIconMaterialItemView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);

        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item_material_only_icon, this, true);

        mIcon = (Image) findComponentById(ResourceTable.Id_icon);
        mMessages = (RoundMessageView) findComponentById(ResourceTable.Id_messages);
        ripple = (RippleLayout) findComponentById(ResourceTable.Id_ripple);
    }

    public void initialization(String title, PixelMap drawable, PixelMap checkedDrawable, boolean tintIcon, int color, int checkedColor) {

        mTitle = title;

        mDefaultColor = color;
        mCheckedColor = checkedColor;

        mTintIcon = tintIcon;

        if (mTintIcon) {
            mDefaultDrawable = Utils.tinting(drawable, mDefaultColor);
            mCheckedDrawable = Utils.tinting(checkedDrawable, mCheckedColor);
        } else {
            mDefaultDrawable = drawable;
            mCheckedDrawable = checkedDrawable;
        }

        mIcon.setPixelMap(mDefaultDrawable);
        ripple.setColor(0xFFFFFF & checkedColor | 0x22000000);

    }

    @Override
    public void setChecked(boolean checked) {
        if (mChecked == checked) {
            return;
        }

        mChecked = checked;

        // 切换颜色
        if (mChecked) {
            mIcon.setPixelMap(mCheckedDrawable);
        } else {
            mIcon.setPixelMap(mDefaultDrawable);
        }
    }

    @Override
    public void setMessageNumber(int number) {
        mMessages.setVisibility(Component.VISIBLE);
        mMessages.setMessageNumber(number);
    }

    @Override
    public void setHasMessage(boolean hasMessage) {
        mMessages.setVisibility(Component.VISIBLE);
        mMessages.setHasMessage(hasMessage);
    }

    @Override
    public void setTitle(String title) {
        // no title
    }

    @Override
    public void setDefaultDrawable(PixelMap drawable) {
        if (mTintIcon) {
            mDefaultDrawable = Utils.tinting(drawable, mDefaultColor);
        } else {
            mDefaultDrawable = drawable;
        }

        if (!mChecked) {
            mIcon.setPixelMap(mDefaultDrawable);
        }
    }

    @Override
    public void setSelectedDrawable(PixelMap drawable) {
        if (mTintIcon) {
            mCheckedDrawable = Utils.tinting(drawable, mCheckedColor);
        } else {
            mCheckedDrawable = drawable;
        }

        if (mChecked) {
            mIcon.setPixelMap(mCheckedDrawable);
        }
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    /**
     * 设置消息圆形的颜色
     *
     * @param color 消息圆形的颜色
     */
    public void setMessageBackgroundColor(int color) {
        mMessages.tintMessageBackground(color);
    }

    /**
     * 设置消息数据的颜色
     *
     * @param color 消息数据的颜色
     */
    public void setMessageNumberColor(int color) {
        mMessages.setMessageNumberColor(color);
    }
}
