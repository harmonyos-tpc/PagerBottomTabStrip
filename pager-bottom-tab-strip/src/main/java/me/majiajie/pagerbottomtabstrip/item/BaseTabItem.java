package me.majiajie.pagerbottomtabstrip.item;

import ohos.agp.components.AttrSet;
import ohos.agp.components.DependentLayout;
import ohos.app.Context;
import ohos.media.image.PixelMap;

/**
 * 所有自定义Item都必须继承此类
 */
public abstract class BaseTabItem extends DependentLayout {

    public BaseTabItem(Context context) {
        super(context);
    }

    public BaseTabItem(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    public BaseTabItem(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * 设置选中状态
     *
     * @param checked 是否选中
     */
    public abstract void setChecked(boolean checked);

    /**
     * 设置消息数字。注意：一般数字需要大于0才会显示
     *
     * @param number 数字
     */
    public abstract void setMessageNumber(int number);

    /**
     * 设置是否显示无数字的小圆点。注意：如果消息数字不为0，优先显示带数字的圆
     *
     * @param hasMessage 是否显示无数字的小圆点
     */
    public abstract void setHasMessage(boolean hasMessage);

    /**
     * 设置标题
     *
     * @param title 标题
     */
    public abstract void setTitle(String title);

    /**
     * 设置未选中状态下的图标
     *
     * @param drawable 未选中状态下的图标
     */
    public abstract void setDefaultDrawable(PixelMap drawable);

    /***
     * 设置选中状态下的图标
     * @param drawable 选中状态下的图标
     */
    public abstract void setSelectedDrawable(PixelMap drawable);

    /**
     * 获取标题文字
     *
     * @return 标题文字
     */
    public abstract String getTitle();

    /**
     * 已选中的状态下再次点击时触发
     */
    public void onRepeat() {
    }

}
