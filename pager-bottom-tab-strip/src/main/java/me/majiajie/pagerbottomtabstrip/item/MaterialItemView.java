package me.majiajie.pagerbottomtabstrip.item;

import me.majiajie.pagerbottomtabstrip.ResourceTable;
import me.majiajie.pagerbottomtabstrip.internal.RoundMessageView;
import me.majiajie.pagerbottomtabstrip.internal.Utils;
import me.majiajie.pagerbottomtabstrip.util.MyValueAnimator;
import me.majiajie.pagerbottomtabstrip.widget.RippleLayout;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.media.image.PixelMap;

/**
 * 材料设计风格项
 */
public class MaterialItemView extends BaseTabItem implements Component.EstimateSizeListener {

    private final RoundMessageView mMessages;
    private final Text mLabel;
    private final Text mLabel1;
    private final Image mIcon;
    private final RippleLayout rippleLayout;

    private PixelMap mDefaultDrawable;
    private PixelMap mCheckedDrawable;

    private int mDefaultColor;
    private int mCheckedColor;

    private final int mTranslation;
    private final int mTranslationHideTitle;
    private final int mTopMargin;
    private final int mTopMarginHideTitle;
    private final int mBottomMargin;

    private boolean mHideTitle;
    private boolean mChecked;

    private MyValueAnimator mAnimator;
    private float mAnimatorValue = 1f;

    private boolean mIsMeasured = false;

    private boolean mTintIcon = true;

    public MaterialItemView(Context context) {
        this(context, null);
    }

    public MaterialItemView(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public MaterialItemView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);

        final float scale = context.getResourceManager().getDeviceCapability().screenDensity / 160;

        mTranslation = (int) (scale * 2);
        mTranslationHideTitle = (int) (scale * 10);
        mTopMargin = (int) (scale * 8);
        mTopMarginHideTitle = (int) (scale * 16);
        mBottomMargin = (int) (scale * 7);

        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item_material, this, true);

        mIcon = (Image) findComponentById(ResourceTable.Id_icon);
        mLabel = (Text) findComponentById(ResourceTable.Id_label);
        mLabel1 = (Text) findComponentById(ResourceTable.Id_label1);
        mMessages = (RoundMessageView) findComponentById(ResourceTable.Id_messages);
        rippleLayout = (RippleLayout) findComponentById(ResourceTable.Id_ripple);
        setEstimateSizeListener(this);
    }

    public void initialization(String title, PixelMap drawable, PixelMap checkedDrawable, boolean tintIcon, int color, int checkedColor) {
        mTintIcon = tintIcon;

        mDefaultColor = color;
        mCheckedColor = checkedColor;

        if (mTintIcon) {
            mDefaultDrawable = Utils.tinting(drawable, mDefaultColor);
            mCheckedDrawable = Utils.tinting(checkedDrawable, mCheckedColor);
        } else {
            mDefaultDrawable = drawable;
            mCheckedDrawable = checkedDrawable;
        }

        mLabel.setText(title);
        mLabel.setTextColor(new Color(color));

        mIcon.setPixelMap(mDefaultDrawable);

        mAnimator = MyValueAnimator.ofFloat(0.0f, 1.0f);
        mAnimator.setDuration(115L);
        mAnimator.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        mAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                mAnimatorValue = value;
                if (mHideTitle) {
                    mIcon.setMarginTop(mTopMarginHideTitle - (int) (mTranslationHideTitle * mAnimatorValue));
                } else {
                    mIcon.setMarginTop(mTopMargin - (int) (mTranslation * mAnimatorValue));
                }
                mLabel.setTextSize((int) (12f + mAnimatorValue * 2f), Text.TextSizeType.FP);
                mLabel1.setTextSize((int) (12f + mAnimatorValue * 100f), Text.TextSizeType.FP);
            }
        });
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        mIsMeasured = true;
        return false;
    }

    /**
     * 尺寸发生变化时调用监听，处理水波纹
     *
     * @param width  控件的宽，即水波纹的最大宽
     * @param height 控件的高，即水波纹的最大高
     */
    public void onSizeChanged(int width, int height) {
        rippleLayout.onSizeChanged(width, height);
    }

    @Override
    public void setChecked(boolean checked) {
        mChecked = checked;

        if (mHideTitle) {
            mLabel.setVisibility(mChecked ? Component.VISIBLE : Component.INVISIBLE);
        }

        if (mIsMeasured) {
            // 切换动画
            if (mChecked) {
                mAnimator.start();
            } else {
                mAnimator.reverse();
            }
        } else { // 布局还未测量完成，第一次初始化
            if (mChecked) { // 选中状态的时候，文字必定显示的
                mIcon.setMarginTop(mTopMargin);
                mLabel.setTextSize(14, Text.TextSizeType.FP);
            } else { // 未选中状态时，文字可能显示也可能不显示
                if (mHideTitle) {
                    mIcon.setMarginTop(mTopMarginHideTitle);
                } else {
                    mIcon.setMarginTop(mTopMargin);
                }
                mLabel.setTextSize(12, Text.TextSizeType.FP);
            }
            mLabel.setMarginBottom(mBottomMargin);
        }

        // 切换颜色
        if (mChecked) {
            mIcon.setPixelMap(mCheckedDrawable);
            mLabel.setTextColor(new Color(mCheckedColor));
        } else {
            mIcon.setPixelMap(mDefaultDrawable);
            mLabel.setTextColor(new Color(mDefaultColor));
        }

    }

    @Override
    public void setMessageNumber(int number) {
        mMessages.setVisibility(Component.VISIBLE);
        mMessages.setMessageNumber(number);
        mMessages.setMarginLeft((rippleLayout.getWidth() - mMessages.getWidth()) / 2 + Utils.vpToPx(getContext(), 14));
    }

    @Override
    public void setHasMessage(boolean hasMessage) {
        mMessages.setVisibility(Component.VISIBLE);
        mMessages.setHasMessage(hasMessage);
    }

    @Override
    public void setTitle(String title) {
        mLabel.setText(title);
    }

    @Override
    public void setDefaultDrawable(PixelMap drawable) {
        if (mTintIcon) {
            mDefaultDrawable = Utils.tinting(drawable, mDefaultColor);
        } else {
            mDefaultDrawable = drawable;
        }

        if (!mChecked) {
            mIcon.setPixelMap(mDefaultDrawable);
        }
    }

    @Override
    public void setSelectedDrawable(PixelMap drawable) {
        if (mTintIcon) {
            mCheckedDrawable = Utils.tinting(drawable, mCheckedColor);
        } else {
            mCheckedDrawable = drawable;
        }

        if (mChecked) {
            mIcon.setPixelMap(mCheckedDrawable);
        }
    }

    @Override
    public String getTitle() {
        return mLabel.getText().toString();
    }

    /**
     * 获取动画运行值[0,1]
     *
     * @return 动画运行值[0, 1]
     */
    public float getAnimValue() {
        return mAnimatorValue;
    }

    /**
     * 设置是否隐藏文字
     *
     * @param hideTitle 是否隐藏文字
     */
    public void setHideTitle(boolean hideTitle) {
        mHideTitle = hideTitle;
    }

    /**
     * 设置消息圆形的颜色
     *
     * @param color 消息圆形的颜色,rgba
     */
    public void setMessageBackgroundColor(int color) {
        mMessages.tintMessageBackground(color);
    }

    /**
     * 设置消息数据的颜色
     *
     * @param color 消息数据的颜色
     */
    public void setMessageNumberColor(int color) {
        mMessages.setMessageNumberColor(color);
    }

    /**
     * 设置是否具有水波纹背景效果
     *
     * @param isRipple 是否具有水波纹背景效果
     */
    public void isRipple(boolean isRipple) {
        rippleLayout.isRipple(isRipple);
    }

    /**
     * 设置水波纹颜色
     *
     * @param color argb
     */
    public void setRippleColor(int color) {
        rippleLayout.setColor(color);
    }

}
