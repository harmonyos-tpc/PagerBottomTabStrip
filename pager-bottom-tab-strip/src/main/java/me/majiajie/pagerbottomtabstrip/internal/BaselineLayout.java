package me.majiajie.pagerbottomtabstrip.internal;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;

/**
 * A simple ViewGroup that aligns all the views inside on a baseline. Note: bottom padding for this
 * view will be measured starting from the baseline.
 */
public class BaselineLayout extends ComponentContainer implements ComponentContainer.ArrangeListener {

    private int mBaseline = -1;

    public BaselineLayout(Context context) {
        this(context, null);
    }

    public BaselineLayout(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public BaselineLayout(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setArrangeListener(this);
    }

    @Override
    public boolean onArrange(int left, int top, int right, int bottom) {
        final int count = getChildCount();
        final int parentLeft = getPaddingLeft();
        final int parentRight = right - left - getPaddingRight();
        final int parentContentWidth = parentRight - parentLeft;
        final int parentTop = getPaddingTop();

        for (int i = 0; i < count; i++) {
            final Component child = getComponentAt(i);
            if (child.getVisibility() == HIDE) {
                continue;
            }

            final int width = child.getEstimatedWidth();
            final int height = child.getEstimatedHeight();

            final int childLeft = parentLeft + (parentContentWidth - width) / 2;
            final int childTop;
            childTop = parentTop;

            child.arrange(childLeft, childTop, childLeft + width, childTop + height);
        }
        return false;
    }

}
