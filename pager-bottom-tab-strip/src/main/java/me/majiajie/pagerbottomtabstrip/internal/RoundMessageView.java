package me.majiajie.pagerbottomtabstrip.internal;

import java.util.Locale;

import me.majiajie.pagerbottomtabstrip.ResourceTable;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class RoundMessageView extends StackLayout {
    private final Component mOval;
    private final Text mMessages;

    private int mMessageNumber;
    private boolean mHasMessage;

    public RoundMessageView(Context context) {
        this(context, null);
    }

    public RoundMessageView(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public RoundMessageView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_round_message_view, this, true);

        mOval = findComponentById(ResourceTable.Id_oval);
        mMessages = (Text) findComponentById(ResourceTable.Id_msg);
        mMessages.setFont(Font.DEFAULT_BOLD);
        mMessages.setTextSize(10, Text.TextSizeType.VP);
    }

    public void setMessageNumber(int number) {
        mMessageNumber = number;

        if (mMessageNumber > 0) {
            mOval.setVisibility(Component.INVISIBLE);
            mMessages.setVisibility(Component.VISIBLE);

            if (mMessageNumber < 10) {
                mMessages.setTextSize(12, Text.TextSizeType.VP);
            } else {
                mMessages.setTextSize(10, Text.TextSizeType.VP);
            }

            if (mMessageNumber <= 99) {
                mMessages.setText(String.format(Locale.ENGLISH, "%d", mMessageNumber));
            } else {
                mMessages.setText(String.format(Locale.ENGLISH, "%d+", 99));
            }
        } else {
            mMessages.setVisibility(Component.INVISIBLE);
            if (mHasMessage) {
                mOval.setVisibility(Component.VISIBLE);
            }
        }
    }

    public void setHasMessage(boolean hasMessage) {
        mHasMessage = hasMessage;

        if (hasMessage) {
            mOval.setVisibility(mMessageNumber > 0 ? Component.INVISIBLE : Component.VISIBLE);
        } else {
            mOval.setVisibility(Component.INVISIBLE);
        }
    }

    /**
     * 设置消息圆点的颜色
     *
     * @param color 消息圆形的颜色,rgba
     */
    public void tintMessageBackground(int color) {
        Element drawable = Utils.tinting(new ShapeElement(getContext(), ResourceTable.Graphic_round), color);
        mOval.setBackground(drawable);
        mMessages.setBackground(drawable);
    }

    public void setMessageNumberColor(int color) {
        mMessages.setTextColor(new Color(color));
    }

    public int getMessageNumber() {
        return mMessageNumber;
    }

    public boolean hasMessage() {
        return mHasMessage;
    }


}
