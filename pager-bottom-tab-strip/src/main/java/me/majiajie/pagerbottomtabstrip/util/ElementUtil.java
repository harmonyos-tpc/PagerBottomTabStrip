/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.majiajie.pagerbottomtabstrip.util;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 背景工具类
 */
public class ElementUtil {

    /**
     * 获取ShapeElement,用于设置背景
     *
     * @param bgColor 适用于不带透明度的6位背景色
     * @return 纯色背景
     */
    public static ShapeElement getShapeElementWith6(int bgColor) {
        if (bgColor < 0 && bgColor >= -0xffffff) {
            bgColor = bgColor + 0xffffff + 1;
        }
        if (bgColor > 0 && bgColor <= 0xffffff) {
            bgColor = alphaColor(bgColor, 1.0f);
        }
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(bgColor));
        return shapeElement;
    }

    /**
     * 获取ShapeElement,用于设置背景
     *
     * @param bgColor 适用于带透明度的8位背景色
     * @return 纯色背景
     */
    public static ShapeElement getShapeElementWith8(int bgColor) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(bgColor));
        return shapeElement;
    }

    /**
     * 通过资源id获取color.json文件中的颜色
     *
     * @param context    上下文
     * @param resColorId 颜色的资源id
     * @return 颜色值
     */
    public static int getColor(Context context, int resColorId) {
        try {
            String strColor = context.getResourceManager().getElement(resColorId).getString();
            if (strColor.length() == 7) {
                return context.getResourceManager().getElement(resColorId).getColor();
            } else if (strColor.length() == 9) {
                return Color.getIntColor(strColor);
            } else {
                return 0x000000;
            }
        } catch (Exception e) {
            Logger.getLogger(ElementUtil.class.getName()).log(Level.SEVERE, e.getMessage());
        }
        return 0x000000;
    }

    /**
     * 把一个rgb颜色和一个透明度合并成一个rgba，用于设置背景色的颜色
     *
     * @param rgbColor rgb颜色
     * @param alpha    0:完全透明，1：不透明
     * @return rgba颜色值
     */
    public static int alphaColor(int rgbColor, float alpha) {
        if (rgbColor < 0) {
            rgbColor = rgbColor + 0xffffff + 1;
        }
        int color = Math.max(0, rgbColor) << 8;
        int alpha255 = (int) (alpha * 255);
        return color + alpha255;
    }

    /**
     * 获取一个圆角背景
     *
     * @param color  颜色值
     * @param radius 角度
     * @return 圆角背景
     */
    public static ShapeElement createDrawable(int color, float radius) {
        ShapeElement drawable = new ShapeElement();
        drawable.setShape(ShapeElement.RECTANGLE);
        drawable.setRgbColor(new RgbColor(color));
        drawable.setCornerRadius(radius);
        return drawable;
    }

    /**
     * 获取一个圆角背景
     *
     * @param color    颜色值
     * @param tlRadius 左上角的角度
     * @param trRadius 右上角的角度
     * @param brRadius 右下角的角度
     * @param blRadius 左下角的角度
     * @return 圆角背景
     */
    public static ShapeElement createDrawable(int color, float tlRadius, float trRadius, float brRadius, float blRadius) {
        ShapeElement drawable = new ShapeElement();
        drawable.setShape(ShapeElement.RECTANGLE);
        drawable.setRgbColor(new RgbColor(color));
        drawable.setCornerRadiiArray(new float[]{
                tlRadius, tlRadius,
                trRadius, trRadius,
                brRadius, brRadius,
                blRadius, blRadius});
        return drawable;
    }

}
