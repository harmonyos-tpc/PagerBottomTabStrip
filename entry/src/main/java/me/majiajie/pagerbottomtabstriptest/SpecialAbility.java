package me.majiajie.pagerbottomtabstriptest;

import me.majiajie.pagerbottomtabstrip.NavigationController;
import me.majiajie.pagerbottomtabstrip.PageNavigationView;
import me.majiajie.pagerbottomtabstrip.item.BaseTabItem;
import me.majiajie.pagerbottomtabstriptest.custom.SpecialTab;
import me.majiajie.pagerbottomtabstriptest.custom.SpecialTabRound;
import me.majiajie.pagerbottomtabstriptest.other.MyViewPagerAdapter;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.PageSlider;

/**
 * Created by mjj on 2017/6/25
 */
public class SpecialAbility extends Ability {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_special);

        PageNavigationView tab = (PageNavigationView) findComponentById(ResourceTable.Id_tab);
        PageSlider pageSlider = (PageSlider) findComponentById(ResourceTable.Id_pageSlider);

        NavigationController navigationController = tab.custom()
                .addItem(newItem(ResourceTable.Media_ic_restore_gray, ResourceTable.Media_ic_restore_teal, "Recents"))
                .addItem(newItem(ResourceTable.Media_ic_favorite_gray, ResourceTable.Media_ic_favorite_teal, "Favorites"))
                .addItem(newRoundItem(ResourceTable.Media_ic_nearby_gray, ResourceTable.Media_ic_nearby_teal, "Nearby"))
                .addItem(newItem(ResourceTable.Media_ic_favorite_gray, ResourceTable.Media_ic_favorite_teal, "Favorites"))
                .addItem(newItem(ResourceTable.Media_ic_restore_gray, ResourceTable.Media_ic_restore_teal, "Recents"))
                .build();

        pageSlider.setProvider(new MyViewPagerAdapter(getContext(), navigationController.getItemCount()));

        // 自动适配PageSlider页面切换
        navigationController.setupWithViewPager(pageSlider);
    }


    /**
     * 创建一个正常tab
     *
     * @param drawable        未选中时的时的元素资源id
     * @param checkedDrawable 选中时的时的元素资源id
     * @param text            文字
     * @return 正常tab
     */
    private BaseTabItem newItem(int drawable, int checkedDrawable, String text) {
        SpecialTab mainTab = new SpecialTab(this);
        mainTab.initialize(drawable, checkedDrawable, text);
        mainTab.setTextDefaultColor(0xFF888888);
        mainTab.setTextCheckedColor(0xFF009688);
        return mainTab;
    }

    /**
     * 创建一个圆形tab
     *
     * @param drawable        未选中时的时的元素资源id
     * @param checkedDrawable 选中时的时的元素资源id
     * @param text            文字
     * @return 圆形tab
     */
    private BaseTabItem newRoundItem(int drawable, int checkedDrawable, String text) {
        SpecialTabRound mainTab = new SpecialTabRound(this);
        mainTab.initialize(drawable, checkedDrawable, text);
        mainTab.setTextDefaultColor(0xFF888888);
        mainTab.setTextCheckedColor(0xFF009688);
        return mainTab;
    }

}
