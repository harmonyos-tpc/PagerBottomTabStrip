package me.majiajie.pagerbottomtabstriptest;

import me.majiajie.pagerbottomtabstrip.NavigationController;
import me.majiajie.pagerbottomtabstrip.PageNavigationView;
import me.majiajie.pagerbottomtabstrip.internal.Utils;
import me.majiajie.pagerbottomtabstrip.util.EventUtil;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ListContainer;
import ohos.agp.components.PageSlider;
import ohos.agp.components.PageSliderProvider;
import ohos.agp.components.Text;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;

/**
 * 控制导航栏显示隐藏演示页面
 */
public class HideAbility extends Ability {

    static NavigationController mNavigationController;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_layout_horizontal);

        PageNavigationView tab = (PageNavigationView) findComponentById(ResourceTable.Id_tab);
        PageSlider pageSlider = (PageSlider) findComponentById(ResourceTable.Id_pageSlider);

        mNavigationController = tab.material()
                .addItem(ResourceTable.Media_ic_restore_teal, "Recents", getColor(ResourceTable.Color_colorPrimary))
                .addItem(ResourceTable.Media_ic_favorite_teal, "Favorites", getColor(ResourceTable.Color_colorPrimary))
                .addItem(ResourceTable.Media_ic_nearby_teal, "Nearby", getColor(ResourceTable.Color_colorPrimary))
                .build();

        pageSlider.setProvider(new TestViewPagerAdapter());

        // 自动适配PageSlider页面切换
        mNavigationController.setupWithViewPager(pageSlider);

    }

    /**
     * 监听列表的滑动来控制底部导航栏的显示与隐藏
     */
    private static class ListTouchListener implements Component.TouchEventListener {

        private float oldY;

        @Override
        public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
            switch (touchEvent.getAction()) {
                case TouchEvent.PRIMARY_POINT_DOWN:
                    oldY = EventUtil.getRawY(touchEvent);
                    return true;
                case TouchEvent.POINT_MOVE:
                    float dy = oldY - EventUtil.getRawY(touchEvent);
                    if (dy > 8) { // 列表向上滑动
                        mNavigationController.hideBottomLayout();
                    } else if (dy < -8) { // 列表向下滑动
                        mNavigationController.showBottomLayout();
                    }
                    oldY = EventUtil.getRawY(touchEvent);
                    return true;
                default:
                    break;
            }
            return false;
        }
    }

    // 下面几个类都是为了测试写的
    private class TestViewPagerAdapter extends PageSliderProvider {

        private ArrayList<TestFragment> fragments;

        public TestViewPagerAdapter() {
            fragments = new ArrayList<>();
        }

        @Override
        public int getCount() {
            return mNavigationController.getItemCount();
        }

        @Override
        public Object createPageInContainer(ComponentContainer componentContainer, int position) {
            TestFragment fragment = null;
            if (fragments.size() - 1 >= position) {
                fragment = fragments.get(position);
            }
            if (fragment == null) {
                fragment = new TestFragment(componentContainer.getContext());
                fragment.setTouchEventListener(new ListTouchListener());
                fragments.add(fragment);
            }
            componentContainer.addComponent(fragment);
            return fragment;
        }

        @Override
        public void destroyPageFromContainer(ComponentContainer componentContainer, int position, Object object) {
            componentContainer.removeComponent(fragments.get(position));
        }

        @Override
        public boolean isPageMatchToObject(Component component, Object object) {
            return false;
        }

    }

    private static class TestFragment extends ListContainer {

        public TestFragment(Context context) {
            super(context);
            setItemProvider(new TestAdapter());
            setLayoutConfig(new LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_PARENT));
        }

    }

    private static class TestAdapter extends BaseItemProvider {

        @Override
        public int getCount() {
            return 100;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
            int padding = Utils.vpToPx(componentContainer.getContext(), 16);
            Text textView = new Text(componentContainer.getContext());
            textView.setText(String.valueOf(position));
            textView.setTextSize(14, Text.TextSizeType.FP);
            textView.setPadding(padding, padding, padding, padding);
            return textView;
        }

    }

}