package me.majiajie.pagerbottomtabstriptest.other;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;
import ohos.app.Context;

import java.util.ArrayList;

/**
 * PageSlider适配器
 */
public class MyViewPagerAdapter extends PageSliderProvider {

    private Context context;
    private int size;
    private ArrayList<AFragment> aFragments = new ArrayList<>();

    public MyViewPagerAdapter(Context context, int size) {
        this.context = context;
        this.size = size;
        for (int i = 0; i < size; i++) {
            aFragments.add(AFragment.newInstance(context, i + ""));
        }
    }

    @Override
    public int getCount() {
        return size;
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int position) {
        AFragment fragment = aFragments.get(position);
        componentContainer.addComponent(fragment);
        return fragment;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int position, Object object) {
        componentContainer.removeComponent(aFragments.get(position));
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object object) {
        return false;
    }

}
