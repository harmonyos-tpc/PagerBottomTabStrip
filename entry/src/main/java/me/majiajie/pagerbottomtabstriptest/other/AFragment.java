package me.majiajie.pagerbottomtabstriptest.other;

import me.majiajie.pagerbottomtabstrip.util.ElementUtil;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;

/**
 * 用于给PageSlider创建页面
 */
public class AFragment extends StackLayout {

    private AFragment(Context context, String content) {
        super(context);
        setLayoutConfig(new LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_PARENT));
        Text textView = new Text(context);
        textView.setTextSize(30, Text.TextSizeType.FP);
        textView.setTextAlignment(TextAlignment.CENTER);
        textView.setText(String.format("Test\n\n%s", content));
        textView.setBackground(ElementUtil.getShapeElementWith8(0xecececff));
        addComponent(textView, new LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_PARENT));
    }

    /**
     * 构建一个AFragment
     *
     * @param context 上下文
     * @param content AFragment要显示的文字
     * @return AFragment
     */
    public static AFragment newInstance(Context context, String content) {
        return new AFragment(context, content);
    }

}
