package me.majiajie.pagerbottomtabstriptest;

import me.majiajie.pagerbottomtabstrip.NavigationController;
import me.majiajie.pagerbottomtabstrip.PageNavigationView;
import me.majiajie.pagerbottomtabstrip.item.BaseTabItem;
import me.majiajie.pagerbottomtabstriptest.custom.OnlyIconItemView;
import me.majiajie.pagerbottomtabstriptest.custom.TestRepeatTab;
import me.majiajie.pagerbottomtabstriptest.other.MyViewPagerAdapter;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.PageSlider;

/**
 * 水平自定义导航栏2演示页面
 */
public class Custom2Ability extends Ability {

    NavigationController mNavigationController;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_layout_horizontal);

        PageNavigationView tab = (PageNavigationView) findComponentById(ResourceTable.Id_tab);
        PageSlider pageSlider = (PageSlider) findComponentById(ResourceTable.Id_pageSlider);

        mNavigationController = tab.custom()
                .addItem(newItemTest(ResourceTable.Media_ic_restore_gray, ResourceTable.Media_ic_restore_teal))
                .addItem(newItem(ResourceTable.Media_ic_favorite_gray, ResourceTable.Media_ic_favorite_teal))
                .addItem(newItem(ResourceTable.Media_ic_nearby_gray, ResourceTable.Media_ic_nearby_teal))
                .build();

        pageSlider.setProvider(new MyViewPagerAdapter(getContext(), 4));

        // 自动适配PageSlider页面切换
        mNavigationController.setupWithViewPager(pageSlider);

    }

    // 创建一个Item
    private BaseTabItem newItem(int drawable, int checkedDrawable) {
        OnlyIconItemView onlyIconItemView = new OnlyIconItemView(this);
        onlyIconItemView.initialize(drawable, checkedDrawable);
        return onlyIconItemView;
    }

    // 创建一个Item(测试重复点击的方法)
    private BaseTabItem newItemTest(int drawable, int checkedDrawable) {
        TestRepeatTab testRepeatTab = new TestRepeatTab(this);
        testRepeatTab.initialize(drawable, checkedDrawable);
        return testRepeatTab;
    }

}