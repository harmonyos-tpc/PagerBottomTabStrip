package me.majiajie.pagerbottomtabstriptest;

import me.majiajie.pagerbottomtabstrip.NavigationController;
import me.majiajie.pagerbottomtabstrip.PageNavigationView;
import me.majiajie.pagerbottomtabstrip.item.BaseTabItem;
import me.majiajie.pagerbottomtabstrip.item.NormalItemView;
import me.majiajie.pagerbottomtabstriptest.other.MyViewPagerAdapter;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.PageSlider;
import ohos.agp.utils.Color;

/**
 * 水平自定义导航栏演示页面
 */
public class CustomAbility extends Ability {

    NavigationController mNavigationController;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_layout_horizontal);

        PageNavigationView tab = (PageNavigationView) findComponentById(ResourceTable.Id_tab);
        PageSlider pageSlider = (PageSlider) findComponentById(ResourceTable.Id_pageSlider);

        mNavigationController = tab.custom()
                .addItem(newItem(ResourceTable.Media_ic_restore_gray, ResourceTable.Media_ic_restore_teal, "Recents"))
                .addItem(newItem(ResourceTable.Media_ic_favorite_gray, ResourceTable.Media_ic_favorite_teal, "Favorites"))
                .addItem(newItem(ResourceTable.Media_ic_nearby_gray, ResourceTable.Media_ic_nearby_teal, "Nearby"))
                .build();

        pageSlider.setProvider(new MyViewPagerAdapter(getContext(), 4));

        // 自动适配PageSlider页面切换
        mNavigationController.setupWithViewPager(pageSlider);

        // 设置消息数
        mNavigationController.setMessageNumber(1, 8);

        // 设置显示小圆点
        mNavigationController.setHasMessage(0, true);

    }

    // 创建一个Item
    private BaseTabItem newItem(int drawable, int checkedDrawable, String text) {
        NormalItemView normalItemView = new NormalItemView(this);
        normalItemView.initialize(drawable, checkedDrawable, text);
        normalItemView.setTextDefaultColor(Color.GRAY.getValue());
        normalItemView.setTextCheckedColor(0xFF009688);
        return normalItemView;
    }

}