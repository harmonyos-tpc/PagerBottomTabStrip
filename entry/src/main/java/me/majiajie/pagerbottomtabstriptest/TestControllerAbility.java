package me.majiajie.pagerbottomtabstriptest;

import java.util.ArrayList;
import java.util.List;

import me.majiajie.pagerbottomtabstrip.NavigationController;
import me.majiajie.pagerbottomtabstrip.PageNavigationView;
import me.majiajie.pagerbottomtabstrip.util.ResourceUtil;
import me.majiajie.pagerbottomtabstriptest.other.MyViewPagerAdapter;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.PageSlider;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.media.image.PixelMap;

/**
 * 测试导航栏控制类
 */
public class TestControllerAbility extends Ability implements Component.ClickedListener {

    private TextField mEdtIndex;
    private PageSlider pageSlider;
    private PageNavigationView mTab;

    private NavigationController mNavigationController;

    private final List<Integer> mMessageNumberList = new ArrayList<>();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_test_controller);
        initView();
        initNavigation();
        initEvent();
    }

    private void initView() {
        mEdtIndex = (TextField) findComponentById(ResourceTable.Id_edt_index);
        pageSlider = (PageSlider) findComponentById(ResourceTable.Id_pageSlider);
        mTab = (PageNavigationView) findComponentById(ResourceTable.Id_tab);
    }

    private void initNavigation() {
        mNavigationController = mTab.material()
                .addItem(ResourceTable.Media_ic_ondemand_video, "Movies & TV", getColor(ResourceTable.Color_colorCheckedOndemand))
                .addItem(ResourceTable.Media_ic_audiotrack, "Music", getColor(ResourceTable.Color_colorCheckedAudiotrack))
                .addItem(ResourceTable.Media_ic_book, "Books", getColor(ResourceTable.Color_colorCheckedBook))
                .addItem(ResourceTable.Media_ic_news, "Newsstand", getColor(ResourceTable.Color_colorCheckedNews))
                .enableAnimateLayoutChanges()
                .build();

        MyViewPagerAdapter pagerAdapter = new MyViewPagerAdapter(getContext(), Math.max(5, mNavigationController.getItemCount()));
        pageSlider.setProvider(pagerAdapter);
        pageSlider.setSlidingPossible(false);

        mNavigationController.setupWithViewPager(pageSlider);

        // 初始化消息数字为0
        for (int i = 0; i < pagerAdapter.getCount(); i++) {
            mMessageNumberList.add(0);
        }
    }

    private void initEvent() {
        findComponentById(ResourceTable.Id_btn_addMessageNumber).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn_subtraMessageNumber).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn_add_item).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn_remove_item).setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        int index = getIndex();
        if (index < 0) {
            return;
        }
        if (component.getId() == ResourceTable.Id_btn_addMessageNumber) {
            if (index >= 0 && index < mMessageNumberList.size()) {
                mMessageNumberList.set(index, mMessageNumberList.get(index) + 1);
                mNavigationController.setMessageNumber(index, mMessageNumberList.get(index));
            }
        }
        if (component.getId() == ResourceTable.Id_btn_subtraMessageNumber) {
            if (index >= 0 && index < mMessageNumberList.size()) {
                mMessageNumberList.set(index, Math.max(0, mMessageNumberList.get(index) - 1));
                mNavigationController.setMessageNumber(index, mMessageNumberList.get(index));
            }
        }
        if (component.getId() == ResourceTable.Id_btn_remove_item) {
            if (mNavigationController.removeItem(index)) {
                mMessageNumberList.remove(index);
            } else {
                showToast("移除失败(指定项不存在或者已被选中)");
            }
        }
        if (component.getId() == ResourceTable.Id_btn_add_item) {
            if (mNavigationController.getItemCount() < 5) {
                mMessageNumberList.add(index, 0);
                PixelMap drawable = ResourceUtil.decodeResource(getContext(), ResourceTable.Media_ic_favorite_gray);
                mNavigationController.addMaterialItem(index, drawable, drawable, "NEW", 0xFF880000);
            } else {
                showToast("材料设计模式下，导航栏数量不要超过5个");
            }
        }
    }

    private int getIndex() {
        String text = mEdtIndex.getText();
        int index = Integer.parseInt(text);
        if (text.isEmpty()) {
            showToast("input index");
            return -1;
        }
        return index;
    }

    private void showToast(String str) {
        Context context = getContext();
        Text text = new Text(getContext());
        text.setWidth(ComponentContainer.LayoutConfig.MATCH_PARENT);
        text.setHeight(ComponentContainer.LayoutConfig.MATCH_CONTENT);
        text.setTextSize(48);
        text.setText(str);
        text.setMultipleLine(true);
        text.setTextAlignment(TextAlignment.CENTER);
        DirectionalLayout directionalLayout = new DirectionalLayout(context);
        directionalLayout.setBackground(createElement(0xF0F0F0F0, 50));
        DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig
                (DirectionalLayout.LayoutConfig.MATCH_PARENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        layoutConfig.setMarginBottom(100);
        directionalLayout.setLayoutConfig(layoutConfig);
        directionalLayout.setPadding(20, 30, 20, 30);
        directionalLayout.addComponent(text);
        ToastDialog toastDialog = new ToastDialog(context);
        toastDialog.setContentCustomComponent(directionalLayout).setDuration(2000).setAutoClosable(true)
                .setAlignment(LayoutAlignment.BOTTOM).setTransparent(true).show();
    }

    private ShapeElement createElement(int color, float radius) {
        ShapeElement drawable = new ShapeElement();
        drawable.setShape(ShapeElement.RECTANGLE);
        drawable.setRgbColor(new RgbColor(color));
        drawable.setCornerRadius(radius);
        return drawable;
    }

}
