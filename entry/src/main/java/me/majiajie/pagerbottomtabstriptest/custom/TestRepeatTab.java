package me.majiajie.pagerbottomtabstriptest.custom;

import me.majiajie.pagerbottomtabstrip.util.CustomAnimatorStateChangedListener;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.AttrSet;
import ohos.app.Context;

/**
 * Created by mjj on 2017/10/19
 * <p>
 * 测试重复点击的触发
 * </p>
 */
public class TestRepeatTab extends OnlyIconItemView {

    public TestRepeatTab(Context context) {
        super(context);
    }

    public TestRepeatTab(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    public TestRepeatTab(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    AnimatorProperty mAnimator;

    @Override
    public void onRepeat() {
        super.onRepeat();

        if (mAnimator == null) {
            mAnimator = getIcon().createAnimatorProperty().rotate(-360f).setDuration(375);
            mAnimator.setStateChangedListener(new CustomAnimatorStateChangedListener() {
                @Override
                public void onEnd(Animator animator) {
                    getIcon().setRotation(0);
                }
            });
        }
        if (!mAnimator.isRunning()) {
            mAnimator.start();
        }

    }
}
