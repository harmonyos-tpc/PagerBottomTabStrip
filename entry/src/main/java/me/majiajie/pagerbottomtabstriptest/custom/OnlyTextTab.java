package me.majiajie.pagerbottomtabstriptest.custom;

import me.majiajie.pagerbottomtabstrip.item.BaseTabItem;
import me.majiajie.pagerbottomtabstriptest.ResourceTable;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.media.image.PixelMap;

/**
 * Created by mjj on 2017/9/26
 */
public class OnlyTextTab extends BaseTabItem {

    private final Text mTitle;

    public OnlyTextTab(Context context, String title) {
        super(context);
        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item_only_text, this, true);
        mTitle = (Text) findComponentById(ResourceTable.Id_title);
        mTitle.setText(title);
    }

    @Override
    public void setChecked(boolean checked) {
        mTitle.setTextColor(checked ? new Color(0xFFF4B400) : new Color(0x56000000));
    }

    @Override
    public void setMessageNumber(int number) {
    }

    @Override
    public void setHasMessage(boolean hasMessage) {
    }

    @Override
    public void setTitle(String title) {
        mTitle.setText(title);
    }

    @Override
    public void setDefaultDrawable(PixelMap drawable) {

    }

    @Override
    public void setSelectedDrawable(PixelMap drawable) {

    }

    @Override
    public String getTitle() {
        return mTitle.getText().toString();
    }
}
