package me.majiajie.pagerbottomtabstriptest.custom;

import me.majiajie.pagerbottomtabstrip.item.BaseTabItem;
import me.majiajie.pagerbottomtabstrip.util.ResourceUtil;
import me.majiajie.pagerbottomtabstriptest.ResourceTable;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;
import ohos.media.image.PixelMap;

/**
 * 自定义一个只有图标的Item
 */
public class OnlyIconItemView extends BaseTabItem {

    private Image mIcon;
    private PixelMap mDefaultDrawable;
    private PixelMap mCheckedDrawable;

    private boolean mChecked;

    public OnlyIconItemView(Context context) {
        this(context, null);
    }

    public OnlyIconItemView(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public OnlyIconItemView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);

        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item_only_icon, this, true);
        mIcon = (Image) findComponentById(ResourceTable.Id_icon);
    }

    /**
     * 设置选中时与未选中时的资源
     *
     * @param drawableRes        选中时的资源
     * @param checkedDrawableRes 未选中时的资源
     */
    public void initialize(int drawableRes, int checkedDrawableRes) {
        mDefaultDrawable = ResourceUtil.decodeResource(getContext(),drawableRes);
        mCheckedDrawable = ResourceUtil.decodeResource(getContext(),checkedDrawableRes);
    }

    @Override
    public void setChecked(boolean checked) {
        mIcon.setPixelMap(checked ? mCheckedDrawable : mDefaultDrawable);
        mChecked = checked;
    }

    @Override
    public void setMessageNumber(int number) {
        // 不需要就不用管
    }

    @Override
    public void setHasMessage(boolean hasMessage) {
        // 不需要就不用管
    }

    @Override
    public void setTitle(String title) {
        // 不需要就不用管
    }

    @Override
    public void setDefaultDrawable(PixelMap drawable) {
        mDefaultDrawable = drawable;
        if (!mChecked) {
            mIcon.setPixelMap(drawable);
        }
    }

    @Override
    public void setSelectedDrawable(PixelMap drawable) {
        mCheckedDrawable = drawable;
        if (mChecked) {
            mIcon.setPixelMap(drawable);
        }
    }

    @Override
    public String getTitle() {
        return "no title";
    }

    /**
     * 获取图片控件
     *
     * @return 图片控件
     */
    protected Component getIcon() {
        return mIcon;
    }

}
