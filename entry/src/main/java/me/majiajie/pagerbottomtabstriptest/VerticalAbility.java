package me.majiajie.pagerbottomtabstriptest;

import me.majiajie.pagerbottomtabstrip.NavigationController;
import me.majiajie.pagerbottomtabstrip.PageNavigationView;
import me.majiajie.pagerbottomtabstrip.listener.OnTabItemSelectedListener;
import me.majiajie.pagerbottomtabstriptest.other.MyViewPagerAdapter;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.PageSlider;

/**
 * Created by mjj on 2017/8/3
 */
public class VerticalAbility extends Ability {

    NavigationController mNavigationController;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_layout_vertical);

        PageNavigationView pageBottomTabLayout = (PageNavigationView) findComponentById(ResourceTable.Id_tab);
        PageSlider pageSlider = (PageSlider) findComponentById(ResourceTable.Id_pageSlider);

        mNavigationController = pageBottomTabLayout.material()
                .addItem(ResourceTable.Media_ic_ondemand_video, "Movies & TV", getColor(ResourceTable.Color_colorCheckedOndemand))
                .addItem(ResourceTable.Media_ic_audiotrack, "Music", getColor(ResourceTable.Color_colorCheckedAudiotrack))
                .addItem(ResourceTable.Media_ic_book, "Books", getColor(ResourceTable.Color_colorCheckedBook))
                .addItem(ResourceTable.Media_ic_news, "Newsstand", getColor(ResourceTable.Color_colorCheckedNews))
                .enableVerticalLayout() // 使用垂直布局
                .build();

        pageSlider.setProvider(new MyViewPagerAdapter(getContext(), mNavigationController.getItemCount()));

        // 自动适配PageSlider页面切换
        mNavigationController.setupWithViewPager(pageSlider);

        // 也可以设置Item选中事件的监听
        mNavigationController.addTabItemSelectedListener(new OnTabItemSelectedListener() {
            @Override
            public void onSelected(int index, int old) {

            }

            @Override
            public void onRepeat(int index) {

            }
        });

        // 设置消息圆点
        mNavigationController.setMessageNumber(0, 8);
        mNavigationController.setHasMessage(3, true);

    }

}
