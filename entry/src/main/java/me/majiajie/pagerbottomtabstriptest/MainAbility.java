package me.majiajie.pagerbottomtabstriptest;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Component.ClickedListener;

/**
 * 示例首页
 */
public class MainAbility extends Ability implements ClickedListener {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_toMaterialdesign).setClickedListener(this);
        findComponentById(ResourceTable.Id_toCustom).setClickedListener(this);
        findComponentById(ResourceTable.Id_toCustom2).setClickedListener(this);
        findComponentById(ResourceTable.Id_toHide).setClickedListener(this);
        findComponentById(ResourceTable.Id_toSpecial).setClickedListener(this);
        findComponentById(ResourceTable.Id_toVertical).setClickedListener(this);
        findComponentById(ResourceTable.Id_toCsutomVertical).setClickedListener(this);
        findComponentById(ResourceTable.Id_toTestController).setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_toMaterialdesign:
                startAbility(MaterialDesignActivity.class);
                break;
            case ResourceTable.Id_toCustom:
                startAbility(CustomAbility.class);
                break;
            case ResourceTable.Id_toCustom2:
                startAbility(Custom2Ability.class);
                break;
            case ResourceTable.Id_toHide:
                startAbility(HideAbility.class);
                break;
            case ResourceTable.Id_toSpecial:
                startAbility(SpecialAbility.class);
                break;
            case ResourceTable.Id_toVertical:
                startAbility(VerticalAbility.class);
                break;
            case ResourceTable.Id_toCsutomVertical:
                startAbility(VerticalCustomAbility.class);
                break;
            case ResourceTable.Id_toTestController:
                startAbility(TestControllerAbility.class);
                break;
            default:
                break;
        }
    }

    private void startAbility(Class bAbility) {
        Intent secondIntent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(getBundleName())
                .withAbilityName(bAbility.getName())
                .build();
        secondIntent.setOperation(operation);
        startAbility(secondIntent, 0);
    }

}
