package me.majiajie.pagerbottomtabstriptest;

import me.majiajie.pagerbottomtabstrip.NavigationController;
import me.majiajie.pagerbottomtabstrip.PageNavigationView;
import me.majiajie.pagerbottomtabstriptest.custom.OnlyTextTab;
import me.majiajie.pagerbottomtabstriptest.other.MyViewPagerAdapter;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.PageSlider;

/**
 * Created by mjj on 2017/9/26
 */
public class VerticalCustomAbility extends Ability {

    NavigationController mNavigationController;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_vertical_custom);

        PageNavigationView pageNavigationView = (PageNavigationView) findComponentById(ResourceTable.Id_tab);
        PageSlider pageSlider = (PageSlider) findComponentById(ResourceTable.Id_pageSlider);

        mNavigationController = pageNavigationView.custom()
                .addItem(new OnlyTextTab(this, "A"))
                .addItem(new OnlyTextTab(this, "B"))
                .addItem(new OnlyTextTab(this, "C"))
                .addItem(new OnlyTextTab(this, "D"))
                .addItem(new OnlyTextTab(this, "E"))
                .addItem(new OnlyTextTab(this, "F"))
                .addItem(new OnlyTextTab(this, "G"))
                .addItem(new OnlyTextTab(this, "H"))
                .addItem(new OnlyTextTab(this, "I"))
                .addItem(new OnlyTextTab(this, "J"))
                .addItem(new OnlyTextTab(this, "K"))
                .addItem(new OnlyTextTab(this, "L"))
                .addItem(new OnlyTextTab(this, "M"))
                .addItem(new OnlyTextTab(this, "N"))
                .addItem(new OnlyTextTab(this, "O"))
                .enableVerticalLayout()
                .build();

        pageSlider.setProvider(new MyViewPagerAdapter(getContext(), mNavigationController.getItemCount()));

        // 自动适配PageSlider页面切换
        mNavigationController.setupWithViewPager(pageSlider);

    }
}
