package me.majiajie.pagerbottomtabstriptest;

import me.majiajie.pagerbottomtabstrip.MaterialMode;
import me.majiajie.pagerbottomtabstrip.NavigationController;
import me.majiajie.pagerbottomtabstrip.PageNavigationView;
import me.majiajie.pagerbottomtabstrip.listener.OnTabItemSelectedListener;
import me.majiajie.pagerbottomtabstriptest.other.MyViewPagerAdapter;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.PageSlider;

/**
 * 水平material类型导航栏演示页面
 */
public class MaterialDesignActivity extends Ability {

    NavigationController mNavigationController;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_layout_horizontal);

        PageNavigationView pageBottomTabLayout = (PageNavigationView) findComponentById(ResourceTable.Id_tab);
        PageSlider pageSlider = (PageSlider) findComponentById(ResourceTable.Id_pageSlider);

        mNavigationController = pageBottomTabLayout.material()
                .addItem(ResourceTable.Media_ic_ondemand_video, "Movies & TV", getColor(ResourceTable.Color_colorCheckedOndemand))
                .addItem(ResourceTable.Media_ic_audiotrack, "Music", getColor(ResourceTable.Color_colorCheckedAudiotrack))
                .addItem(ResourceTable.Media_ic_book, "Books", getColor(ResourceTable.Color_colorCheckedBook))
                .addItem(ResourceTable.Media_ic_news, "Newsstand", getColor(ResourceTable.Color_colorCheckedNews))
                .setDefaultColor(0x89FFFFFF) // 未选中状态的颜色
                .setMode(MaterialMode.CHANGE_BACKGROUND_COLOR | MaterialMode.HIDE_TEXT) // 这里可以设置样式模式，总共可以组合出4种效果
                .build();

        pageSlider.setProvider(new MyViewPagerAdapter(getContext(), 4));

        // 自动适配PageSlider页面切换
        mNavigationController.setupWithViewPager(pageSlider);

        // 也可以设置Item选中事件的监听
        mNavigationController.addTabItemSelectedListener(new OnTabItemSelectedListener() {
            @Override
            public void onSelected(int index, int old) {

            }

            @Override
            public void onRepeat(int index) {

            }
        });

    }

}