# PagerBottomTabStrip

页面底部和侧边的导航栏。

- 可以自定义Item进行扩展
- 自带消息圆点
- 可在构建完成之后动态增加/删除导航按钮、修改图标和文字
- 方便适配常用的PageSlider

## 实现效果图

|HORIZONTAL|VERTICAL|
|:---:|:---:|
|<img src="https://gitee.com/HarmonyOS-tpc/PagerBottomTabStrip/raw/master/img/horizontal.jpg" width="75%"/>|<img src="https://gitee.com/HarmonyOS-tpc/PagerBottomTabStrip/raw/master/img/vertical.jpg" width="75%"/>|

|DEFAULT|HIDE_TEXT|
|:---:|:---:|
|<img src="https://gitee.com/HarmonyOS-tpc/PagerBottomTabStrip/raw/master/img/Material1.gif" width="75%"/>|<img src="https://gitee.com/HarmonyOS-tpc/PagerBottomTabStrip/raw/master/img/Material2.gif" width="75%"/>|

|CHANGE_BACKGROUND_COLOR|CHANGE_BACKGROUND_COLOR且HIDE_TEXT|
|:---:|:---:|
|<img src="https://gitee.com/HarmonyOS-tpc/PagerBottomTabStrip/raw/master/img/Material3.gif" width="75%"/>|<img src="https://gitee.com/HarmonyOS-tpc/PagerBottomTabStrip/raw/master/img/Material4.gif" width="75%"/>|


|Custom1|Custom2|
|:---:|:---:|
|<img src="https://gitee.com/HarmonyOS-tpc/PagerBottomTabStrip/raw/master/img/Custom1.gif" width="75%"/>|<img src="https://gitee.com/HarmonyOS-tpc/PagerBottomTabStrip/raw/master/img/Custom2.gif" width="75%"/>|

|Custom3|
|:---:|
|<img src="https://gitee.com/HarmonyOS-tpc/PagerBottomTabStrip/raw/master/img/Custom3.gif" width="75%"/>|

|Hide|TestController|
|:---:|:---:|
|<img src="https://gitee.com/HarmonyOS-tpc/PagerBottomTabStrip/raw/master/img/Hide.gif" width="75%"/>|<img src="https://gitee.com/HarmonyOS-tpc/PagerBottomTabStrip/raw/master/img/TestController.gif" width="75%"/>|

|VerticalMaterial|VerticalCustom|
|:---:|:---:|
|<img src="https://gitee.com/HarmonyOS-tpc/PagerBottomTabStrip/raw/master/img/VerticalMaterial.gif" width="75%"/>|<img src="https://gitee.com/HarmonyOS-tpc/PagerBottomTabStrip/raw/master/img/VerticalCustom.gif" width="75%"/>|

## 集成
```java
allprojects {
    repositories {
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:PagerBottomTabStrip:1.0.1'
```

## 用法
1. 在XML布局文件中添加一个底部导航栏控件
   ```html
        <me.majiajie.pagerbottomtabstrip.PageNavigationView
            ohos:id="$+id:tab"
            ohos:height="56vp"
            ohos:width="match_parent"
            ohos:align_parent_bottom="true"/>
   ```
2. 在Ability中初始化，这里创建一个Material风格的导航栏
   ```java
		PageNavigationView tab = (PageNavigationView) findComponentById(ResourceTable.Id_tab);

        NavigationController mNavigationController = tab.material()
            .addItem(ResourceTable.Media_ic_ondemand_video, "Movies & TV", getColor(ResourceTable.Color_colorCheckedOndemand))
            .addItem(ResourceTable.Media_ic_audiotrack, "Music", getColor(ResourceTable.Color_colorCheckedAudiotrack))
            .addItem(ResourceTable.Media_ic_book, "Books", getColor(ResourceTable.Color_colorCheckedBook))
            .addItem(ResourceTable.Media_ic_news, "Newsstand", getColor(ResourceTable.Color_colorCheckedNews))
            .setDefaultColor(0x89FFFFFF) // 未选中状态的颜色
            .setMode(MaterialMode.CHANGE_BACKGROUND_COLOR | MaterialMode.HIDE_TEXT) // 这里可以设置样式模式，总共可以组合出4 种效果
            .build();
   ```
   这里构建完之后得到了一个NavigationController实例，它可以对导航栏进行后续的控制
3. 如果你使用PageSlider来配合使用，那么可以很简单的适配PageSlider的页面切换
   ```java
		mNavigationController.setupWithViewPager(pageSlider);
   ```
   如果你使用其它的控件，那么可能就需要监听导航栏的Item选中事件来自己做处理了
   ```java
		mNavigationController.addTabItemSelectedListener(new OnTabItemSelectedListener() {
            @Override
            public void onSelected(int index, int old) {
               // 选中时触发
            }

            @Override
            public void onRepeat(int index) {
                // 重复选中时触发
            }
        });
   ```
以上的步骤已经尽可能方便的创建了一个Material风格的底部导航栏，如果要实现自定义风格导航栏请参考entry示例代码。

## 示例entry运行要求
通过DevEco studio,并下载SDK
将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本）

## LICENSE

PagerBottomTabStrip is released under the [Apache 2.0 license](https://gitee.com/openharmony-tpc/PagerBottomTabStrip/blob/master/LICENSE).
```
Copyright 2016 MJJ

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
